import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.io.File;
import java.util.Scanner;

public class CodeWriter {
    public Parser parser=null;
    public String outFileName="";
    private String ans="";
    public CodeWriter(List aList, String outFileName,String ans){
        this.outFileName=outFileName;
        parser=new Parser(aList);
        this.ans=ans.trim().toLowerCase().equals("y")?"//INIT\n"+Traduction.INIT+"\n":"";
    }
    public void getAllCommands() throws IOException {
        for(File f:parser.getVmFiles()){
            String currentFileName=f.getName().replace(".vm","");
            Scanner sc=new Scanner(f);
            while (sc.hasNext()){
                String currentLine=sc.nextLine();
                parser.createCommand(currentLine,currentFileName);
            }
            sc.close();
            File ftest=new File(outFileName);
            ftest.createNewFile();
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<parser.getAllCommands().size();i++){
                sb.append("//"+parser.getAllCommands().get(i).buildCommand()+"\n"+""+parser.parse(parser.getAllCommands().get(i),String.valueOf(i))+"\n");
            }
            PrintWriter w=new PrintWriter(ftest);
            w.print(ans+sb.toString());
            w.close();

        }

    }


}
