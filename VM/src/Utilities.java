import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Utilities {
    public static List<File> getFiles(String path) {
        List<File> allFiles = new ArrayList<File>();
        File f = new File(path);
        if (f.exists()) {
            if (f.getPath().contains(".vm")) {
                allFiles.add(f);
                return allFiles;
            } else if (f.isDirectory()) {
                for (File fs : f.listFiles()) {
                    if (fs.getPath().contains(".vm")) {
                        allFiles.add(fs);
                    }
                }
                return allFiles;

            } else {
                //return empty list
                return allFiles;
            }
        } else {
            System.out.println("invalid path!");
            return null;
        }

    }


}


