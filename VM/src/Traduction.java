
public class Traduction {
    public static String line="";

    public static final String PUSH =
            "@SP\n" +
                    "A=M\n" +
                    "M=D\n" +
                    "@SP\n" +
                    "M=M+1";
    public static final String POP =
            "@SP\n" +
                    "AM=M-1\n"+
                    "D=M\n"+
                    "@R13\n"+
                    "A=M\n"+
                    "M=D";
    public static final String ADD=
            "@SP\n"+
                    "AM=M-1\n"+
                    "D=M\n"+
                    "A=A-1\n"+
                    "M=D+M";

    public static final String SUB=
            "@SP\n"+
                    "AM=M-1\n"+
                    "D=M\n"+
                    "A=A-1\n"+
                    "M=M-D";
    public static final String AND=
            "@SP\n"+
            "AM=M-1\n"+
            "D=M\n"+
            "A=A-1\n"+
            "M=D&M";
    public static final String OR=
            "@SP\n"+
            "AM=M-1\n"+
            "D=M\n"+
            "A=A-1\n"+
            "M=D|M";
    public static final String NOT=
            "@SP\n"+
                    "A=M-1\n"+
                    "M=!M";
    public static final String NEG=
            "@SP\n"+
                    "A=M-1\n"+
                    "M=-M";
    public static String LT(String line){
        return  COMPSUB+"\n"+
                "@TRUE-LT-"+line+"\n"+
                "D;JLT\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=0\n"+
                "@END-LT-"+line+"\n"+
                "0;JMP\n"+
                "(TRUE-LT-"+line+")\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=-1\n"+
                "(END-LT-"+line+")";
    }
    public static String GT(String line){
        return COMPSUB+"\n"+
                "@TRUE-GT-"+line+"\n"+
                "D;JGT\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=0\n"+
                "@END-GT-"+line+"\n"+
                "0;JMP\n"+
                "(TRUE-GT-"+line+")\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=-1\n"+
                "(END-GT-"+line+")";
    }
    public static String EQ(String line){
        return COMPSUB+"\n"+
                "@TRUE-EQ-"+line+"\n"+
                "D;JEQ\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=0\n"+
                "@END-EQ-"+line+"\n"+
                "0;JMP\n"+
                "(TRUE-EQ-"+line+")\n"+
                "@SP\n"+
                "A=M-1\n"+
                "M=-1\n"+
                "(END-EQ-"+line+")";
    }
    private static String genericPush(String location,String index){
        return "@"+index+"\n"+
                "D=A\n"+
                "@"+location+"\n"+
                "A=M\n"+
                "A=D+A\n"+
                "D=M\n"+
                PUSH+"\n";
    }
    private static String genericPop(String location,String index){
        return "@"+index+"\n"+
                "D=A\n"+
                "@"+location+"\n"+
                "A=M\n"+
                "D=D+A\n"+
                "@R13\n"+
                "M=D\n"+
                POP+"\n";
    }
    private static String genericLabel(String label){
        return "("+label+")";
    }
    private static String genericGoto(String label){
        return "@"+label+"\n"+
                "0;JMP";
    }
    private static String genericIfGoTo(String label){
        return "@SP\n"+
                "AM=M-1\n"+
                "D=M\n"+
                "@"+label+"\n"+
                "D;JNE";

    }
    private static String genericFunction(String funcName,String nArgs){
        int n=Integer.parseInt(nArgs);//the numbers of push arguments
        String out="("+funcName+")\n@SP\nA=M\n";//build the local segment in the temporal stack
        for(int i=0;i<n;i++){
            out+="M=0\nA=A+1\n";//Push n zeros to the argument part of the stack
        }
        //now i have to store the new sp topmost value´s address in the SP location
        out+="D=A\n@SP\nM=D";//this will be the new pointer for the fresh generated stack *SP+(A+1*nArgs+1)
        return out;
    }
    private static String genericCall(String funcName,String nLocals,String line){
        //This is the call function translation, all the functions can describe what they do
        String outPut="";
        outPut+=allocateRetAddress(funcName,line)+SaveState+RecalculateArg(nLocals)
                +RecalculateLCL+JumpToFunc(funcName,line);
        return outPut;
    }

    private static String Return(String line){
       String out=getEndFrame+popToArg+recoverSegment("THAT")+
                recoverSegment("THIS")+recoverSegment("ARG")+
                recoverLCL+JumPRetAddress;

        return out;
    }
    private static final String getEndFrame="@LCL\nD=M\n@5\nA=D-A\nD=M\n@R13\nM=D\n";
    private static final String popToArg="@SP\nA=M-1\nD=M\n@ARG\nA=M\nM=D\n@ARG\nD=M+1\n@SP\nM=D\n";
    private static final String recoverLCL="@LCL\nA=M-1\nD=M\n@LCL\nM=D\n";
    private static String recoverSegment(String segmentName){
        return "@LCL\nAM=M-1\nD=M\n@"+segmentName+"\nM=D\n";
    }
    private static final String JumPRetAddress="@R13\nA=M\n0;JMP\n";

   // private static final String storeSP="@SP\nD=M\n@R13\nM=D\n";//this is the current caller stack pointer
    private static String allocateRetAddress(String funcName,String line){
        String out="@"+funcName+"-"+line+"\nD=A\n"+PUSH+"\n";//push the return address of the caller
        return out;
    }
    private static  String RecalculateArg(String nArgs){
        return "@SP\nD=M\n@5\nD=D-A\n@"+nArgs+"\nD=D-A\n@ARG\nM=D\n";//recalculate and set the temporal arg address in the current function´s stack
    }
    private static String JumpToFunc(String funcName,String line){
        return "@"+funcName+"\n0;JMP\n("+funcName+"-"+line+")\n";//this is the label where begins the next command out of the call method
    }
    private static final String RecalculateLCL="@SP\nD=M\n@LCL\nM=D\n";
    private static final String allocateLocal="@LCL\nD=M\n"+PUSH+"\n";//push the address of the caller´s "local" segment
    private static final String allocateArg="@ARG\nD=M\n"+PUSH+"\n";//push the address of the caller´s "argument" segment
    private static final String allocateThis="@THIS\nD=M\n"+PUSH+"\n";//push the address of the caller´s "this" segment
    private static final String allocateThat="@THAT\nD=M\n"+PUSH+"\n";//push the address of the caller´s "that" segment
    private static final String SaveState=allocateLocal+allocateArg+allocateThis+allocateThat;


    public static final String memoryAccess(Command cm){
        String outPut="";
        switch (cm.getCommand()){
            case "push":
                switch (cm.getArg1()){
                    case"constant":
                        outPut+="@"+cm.getArg2()+"\n"+
                                "D=A\n"+PUSH;
                        break;
                    case "local":
                        outPut+=genericPush("LCL",cm.getArg2());
                        break;
                    case "this":
                        outPut+=genericPush("THIS",cm.getArg2());
                        break;
                    case  "that":
                        outPut+=genericPush("THAT",cm.getArg2());
                        break;
                    case "argument":
                        outPut+=genericPush("ARG",cm.getArg2());
                        break;
                    case "temp":
                        outPut+="@"+cm.getArg2()+"\n"+
                                "D=A\n"+
                                "@5"+"\n"+
                                "A=D+A\n"+
                                "D=M\n"+
                                PUSH+"\n";
                        break;
                    case "pointer":
                        String location=cm.getArg2().equals("0")?"THIS":"THAT";
                        outPut+="@"+location+"\n"+
                                "D=M\n"+
                                "@SP\n"+
                                "A=M\n"+
                                "M=D\n"+
                                "@SP\n"+
                                "M=M+1";
                        break;
                    case "static":
                        outPut+=
                                "@"+cm.getCurrentCommandFile()+"."+cm.getArg2()+"\n"+
                                "D=M\n"+
                                "@SP\n"+
                                "A=M\n"+
                                "M=D\n"+
                                "@SP\n"+
                                "M=M+1";

                        break;
                }
                break;
            case "pop":
                switch (cm.getArg1()){
                    case "local":
                        outPut+=genericPop("LCL",cm.getArg2());
                        break;
                    case "this":
                        outPut+=genericPop("THIS",cm.getArg2());
                        break;
                    case "that":
                        outPut+=genericPop("THAT",cm.getArg2());
                        break;
                    case "argument":
                        outPut+=genericPop("ARG",cm.getArg2());
                        break;
                    case "temp":
                        outPut+="@"+cm.getArg2()+"\n"+
                                "D=A\n"+
                                "@5"+"\n"+
                                "D=D+A\n"+
                                "@R13\n"+
                                "M=D\n"+
                                POP+"\n";
                        break;
                    case "pointer":
                        String location=cm.getArg2().equals("0")?"THIS":"THAT";
                        outPut+="@SP\n"+
                                "AM=M-1\n"+
                                "D=M\n"+
                                "@"+location+"\n"+
                                "M=D";

                        break;
                    case "static":
                        outPut+="@SP\n"+
                                "AM=M-1\n"+
                                "D=M\n"+
                                "@"+cm.getCurrentCommandFile()+"."+cm.getArg2()+"\n"+
                                "M=D";
                        break;
                }
                break;
                default:

        }
        return outPut;
    }
    public static String programFlow(Command cm){
        String outPut="";
        switch (cm.getCommand()){
            case "if-goto":
                outPut=genericIfGoTo(cm.getArg1());
                break;
            case "goto":
                outPut=genericGoto(cm.getArg1());
                break;
            case "label":
                outPut=genericLabel(cm.getArg1());
                break;
        }
        return outPut;
    }
    public static String functionCalls(Command cm){
        switch (cm.getCommand())
        {
            case "function":
                return genericFunction(cm.getArg1(),cm.getArg2());

            case "call":
                return genericCall(cm.getArg1(),cm.getArg2(),line);
            case "return":
                return Return(line);
        }
        return "";
    }
    public static final String COMPSUB=
            "@SP\n"+
            "AM=M-1\n"+
            "D=M\n"+
            "A=A-1\n"+
            "D=M-D";
    public static final String INIT=
            "@256\n"+
            "D=A\n"+
            "@SP\n"+
            "M=D\n"+genericCall("Sys.init","0",line);







}
