import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        //ask for the files
        System.out.println("Set the path for the .vm file or .vm file´s directory");
        Scanner sc = new Scanner(System.in);
        String path=sc.nextLine();
        System.out.println("add the sys.init 0 call to the output?[y/anything else for no]");
        Scanner cs=new Scanner(System.in);
        String ans=cs.nextLine();
        List<File> egs = Utilities.getFiles(path);
        if(egs==null){
            return;
        }
        if (egs.size() == 0 ) {
            System.out.println(".vm files not found");
            return;
        }
        String outFileName="";
        if(new File(path).isDirectory()){
            File f=new File(egs.get(0).getParent());
            outFileName=egs.get(0).getParent()+"\\"+f.getName()+".asm";
        }
        else if(new File(path).getPath().contains(".vm")){
            outFileName=egs.get(0).getParent()+"\\"+egs.get(0).getName().replace(".vm",".asm");
        }

        //init
        CodeWriter cw=new CodeWriter(egs,outFileName,ans);
        cw.getAllCommands();
        System.out.println("Done!");

    }
}
