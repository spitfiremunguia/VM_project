import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Parser {
    private List<File>vmFiles=null;
    private List<Command>allCommands=null;
    public Parser(List<File>_vmFiles){
        this.setVmFiles(_vmFiles);
        this.setAllCommands(new ArrayList<Command>());

    }
    public void createCommand(String line,String currentCommandFile){
        //first clean the command of commentaries
        line=line.trim();//take out the spaces
        if(line.contains("//")){
            int commentIndex=line.indexOf("/");
            line=line.substring(0,commentIndex);
        }
        //get type
        line=line.replaceAll("\\s",";");
        String newCommand[]=line.split(Pattern.quote(";"));
        List<String >ss=new ArrayList<>() ;
        for(String s:newCommand){
            if(s.equals("")==false){
                ss.add(s);
            }
        }
        newCommand=null;
        newCommand=ss.toArray(new String[ss.size()]);//get the current commands parts
        String command="";
        String arg1="";
        String arg2="";
        String type="";
        //get the command type, first part
        if(newCommand.length==3){
            String currentCommand=newCommand[0];
            switch (currentCommand){
                case "push":
                    command="push";
                    type="C_PUSH";
                    break;
                case"pop":
                    command="pop";
                    type="C_POP";
                    break;
                case "call":
                    command="call";
                    type="C_CALL";
                    break;
                case "function":
                    command="function";
                    type="C_FUNCTION";
                    break;

            }
            arg1=newCommand[1];
            arg2=newCommand[2];
            Command cm=new Command(command,type,arg1,arg2,currentCommandFile);
            this.getAllCommands().add(cm);
        }
        else if(newCommand.length==2){
            //not now this is for goto or something
            String currentCommand=newCommand[0];
            switch (currentCommand){
                case "goto":
                    command="goto";
                    type="C_GOTO";
                    break;
                case "label":
                    command= "label";
                    type="C_LABEL";
                    break;
                case "if-goto":
                    command="if-goto";
                    type="C_IF";
                    break;
            }
            arg1=newCommand[1];
            Command cm=new Command(command,type,arg1,arg2,currentCommandFile);
            this.getAllCommands().add(cm);

        }
        else if(newCommand.length==1){
            String currentCommand=newCommand[0].toLowerCase();
            Pattern pt=Pattern.compile("(add|sub|eq|neg|gt" +
                    "|lt|and|or|not)");

            if(pt.matcher(currentCommand).matches()){
                command=currentCommand;
                type="C_ARITHMETIC";
            }
            else{
                if(currentCommand.equals("return")){
                    command="return";
                    type="C_RETURN";

                }
            }
            Command cm=new Command(command,type,arg1,arg2,currentCommandFile);
            this.getAllCommands().add(cm);
        }
    }
    public String parse(Command cm,String  line){

        switch (cm.getType()){
            case "C_ARITHMETIC":
                switch (cm.getCommand()){
                    case"add":
                        return Traduction.ADD;
                    case "sub":
                        return Traduction.SUB;
                    case"eq":
                        return Traduction.EQ(line);
                    case "neg":
                        return Traduction.NEG;
                    case "gt":
                        return Traduction.GT(line);
                    case "lt":
                        return Traduction.LT(line);
                    case "and":
                        return Traduction.AND;
                    case "or":
                        return Traduction.OR;
                    case "not":
                        return Traduction.NOT;
                }
                break;
            case "C_PUSH":
                return Traduction.memoryAccess(cm);
            case "C_POP":
                return Traduction.memoryAccess(cm);
                default:
                    return "";
            case "C_IF":
                return Traduction.programFlow(cm);
            case  "C_GOTO":
                return Traduction.programFlow(cm);
            case  "C_LABEL":
                return Traduction.programFlow(cm);
            case "C_FUNCTION":
                Traduction.line=line;
                return Traduction.functionCalls(cm);
            case "C_CALL":
                Traduction.line=line;
                return Traduction.functionCalls(cm);
            case "C_RETURN":
                Traduction.line=line;
                return Traduction.functionCalls(cm);


        }
        return "";
    }


    public List<File> getVmFiles() {
        return vmFiles;
    }

    public void setVmFiles(List<File> vmFiles) {
        this.vmFiles = vmFiles;
    }

    public List<Command> getAllCommands() {
        return allCommands;
    }

    public void setAllCommands(List<Command> allCommands) {
        this.allCommands = allCommands;
    }
}
