public class Command {
    private String command="";
    private String type="";
    private String arg1="";
    private String arg2="";
    private String currentCommandFile="";
    public Command(){

    }
    public Command(String command,String type, String arg1, String arg2,String currentCommandFile){
        this.setCommand(command);
        this.setType(type);
        this.setArg1(arg1);
        this.setArg2(arg2);
        this.setCurrentCommandFile(currentCommandFile);

    }
    public String buildCommand(){
        return  command+" "+arg1+" "+arg2;
    }
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getArg1() {
        return arg1;
    }

    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    public String getArg2() {
        return arg2;
    }

    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    public String getCurrentCommandFile() {
        return currentCommandFile;
    }

    public void setCurrentCommandFile(String currentCommandFile) {
        this.currentCommandFile = currentCommandFile;
    }
}
