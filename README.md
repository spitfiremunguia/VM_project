#                                           Vitual Machine 
## Implementation from Nand2tetris.org 
This project is the implementation of the Nand2tetris's virtual machine for the Hack computer.
More info: http://nand2tetris.org/
### Author
David Munguía-1287416<br>
davidmunguiaamericas@gmail.com

### Requirements
-A version of java installed in your computer <br>
-.vm files to test <br>
-OS with a terminal to run java commands <br>

### How to run
-Donwload this repository in your computer
-First you need the VM.jar file avalible in this project
-Open your command prompt and run this command:
#### java -jar Path of the VM.jar in your computer
#### Example: java -jar C:\Users\david\IdeaProjects\VM_project\VM.jar
-The program should ask you for the path of .vm files <br>
-You can insert the path of a folder that contains .vm files or the path of a single .vm file <br>
-Then, the program will ask you if you want to add the sys.init asm code to the output file <br>
-Enter "y" to add the sys.init code, anything else will be taken as no! <br>
-The output will be in both cases a .asm file with the the directory name or the .vm file name, depends what you enter <br>
-The output file will be in the same folder of the input directory or in the same folder of the input .vm file

### Note
This is the full implementation of the Nand2tetris Vitual Machine
#### What is implemented
- StackArithmetic <br>
- Memory Access <br>
- Program flow <br>
- Functions calls <br>
 

